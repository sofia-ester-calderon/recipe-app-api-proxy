#!/bin/sh

# return failure if one line of the script doesn't work
set -e

# populate the environment vars and write to conf file
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# run with daemon off to run in foreground - necessary for docker
nginx -g 'daemon off;'